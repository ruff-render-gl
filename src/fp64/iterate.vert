#version 400 core
#extension GL_ARB_vertex_attrib_64bit : enable

#define POSITION	0
#define COLOR		3
#define FRAG_COLOR	0

layout(location = POSITION) in dvec4 Position;
layout(location = COLOR) in dvec4 Color;

out block {
	dvec4 Color;
	dvec4 Position;
} Out;

const double er2 = 65536.0;

void main() {
	dvec2 c = Position.xy;
	dvec2 z = Color.xy;
	dvec2 d = Color.zw;
	double zx2 = z.x * z.x;
	double zy2 = z.y * z.y;
	double z2 = zx2 + zy2;
	double z2xy = 2.0 * z.x * z.y;
	double zdzx = z.x * d.x - z.y * d.y;
	double zdzy = z.x * d.y + z.y * d.x;
	double dx = 2.0 * zdzx + 1.0;
	double dy = 2.0 * zdzy;
  double zx = zx2 - zy2 + c.x;
  double zy = z2xy + c.y;
	Out.Position = Position;
	Out.Color = dvec4(zx, zy, dx, dy);
}
