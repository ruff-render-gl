#version 400 core
#extension GL_ARB_vertex_attrib_64bit : enable
#extension GL_NV_gpu_shader5 : enable

#define POSITION	0
#define COLOR		3
#define FRAG_COLOR	0

layout(location = POSITION) in dvec4 Position;
layout(location = COLOR) in dvec4 Color;

out dvec4 OutPosition;
out dvec4 OutColor;

void main() {
  OutPosition = Position;
  OutColor = Color;
}
