#version 400 core
#extension GL_ARB_vertex_attrib_64bit : enable

#define POSITION	0
#define COLOR		3
#define FRAG_COLOR	0

in block {
  vec4 Color;
} In;

layout(location = FRAG_COLOR, index = 0) out vec4 Color;

void main() {
 Color = vec4(In.Color);
}
