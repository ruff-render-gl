#version 400 core
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_vertex_attrib_64bit : enable

#define POSITION	0
#define COLOR		3
#define FRAG_COLOR	0

layout(points) in;
layout(points, max_vertices = 1) out;

layout(location = POSITION) in dvec4 OutPosition[1];
layout(location = COLOR) in dvec4 OutColor[1];

uniform bool escape;

out block {
	dvec4 Position;
	dvec4 Color;
} Out;

const double er2 = 4.0;

void main() {
	dvec2 z = OutColor[0].xy;
	double zx2 = z.x * z.x;
	double zy2 = z.y * z.y;
	double z2 = zx2 + zy2;
  if (escape == !(z2 < er2)) {
		Out.Position = OutPosition[0];
		Out.Color = OutColor[0];
		EmitVertex();
		EndPrimitive();
	}
}
