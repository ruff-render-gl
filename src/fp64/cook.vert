#version 400 core
#extension GL_ARB_vertex_attrib_64bit : enable

#define POSITION	0
#define COLOR		3
#define FRAG_COLOR	0

layout(location = POSITION) in dvec4 Position;
layout(location = COLOR) in dvec4 Color;

out block {
	dvec4 Color;
	dvec4 Position;
} Out;

const float er2 = 65536.0;

uniform double iters;
uniform double scale;

void main() {
	dvec2 c = Position.xy;
	dvec2 z = Color.xy;
	dvec2 dz = Color.zw;
	double zx2 = z.x * z.x;
	double zy2 = z.y * z.y;
	double z2 = zx2 + zy2;
	double z2xy = 2.0 * z.x * z.y;
	double dz2 = dz.x * dz.x + dz.y * dz.y;
  double n = 1.0 + iters - double(log2(log(float(z2)) / log(er2)));
  double d = double(log(float(z2))) * sqrt(z2 / dz2) / scale;
  double a = double(atan(float(z.y), float(z.x)));
	Out.Position = Position;
	Out.Color = dvec4(n, d, a, 1.0);
}
