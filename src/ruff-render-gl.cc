/*
ruff-render-gl -- GPU accelerated tile renderer for ruff
(C) 2011 Claude Heiland-Allen <claude@mathr.co.uk>

based on:

// **********************************
// OpenGL Transform Feedback Separated
// 06/04/2010 - 22/06/2011
// **********************************
// Christophe Riccio
// ogl-samples@g-truc.net
// **********************************
// G-Truc Creation
// www.g-truc.net
// **********************************

*/

#include <glf/glf.hpp>

#ifndef FP
#define FP 64
#endif

#if (FP == 32)
#define SHADER_DIR "fp32/"
const int major = 3;
const int minor = 3;
typedef glm::vec4 vec4;
typedef glm::mat4 mat4;
const mat4 IDENTITY = mat4(1.0f);
typedef float flt;
#define glVertexAttribPointer_(a,b,c,d,e) glVertexAttribPointer(a,b,GL_FLOAT,c,d,e)
#define glUniformMatrix4fv_ glUniformMatrix4fv
#define glUniform1f_ glUniform1f
#else
#if (FP == 64)
#define SHADER_DIR "fp64/"
const int major = 4;
const int minor = 0;
typedef glm::dvec4 vec4;
typedef glm::dmat4 mat4;
const mat4 IDENTITY = mat4(1.0);
typedef double flt;
#define glVertexAttribPointer_(a,b,c,d,e) glVertexAttribLPointer(a,b,GL_DOUBLE,d,e)
#define glUniformMatrix4fv_ glUniformMatrix4dv
#define glUniform1f_ glUniform1d
#else
#error unknown FP
#endif
#endif

typedef vec4 point[2];

namespace
{
  std::string const SAMPLE_NAME = "OpenGL Mandelbrot Set ";
  std::string const VERT_SHADER_SOURCE_ITERATE(SHADER_DIR "iterate.vert");
  std::string const VERT_SHADER_SOURCE_ESCAPE (SHADER_DIR "escape.vert");
  std::string const GEOM_SHADER_SOURCE_ESCAPE (SHADER_DIR "escape.geom");
  std::string const VERT_SHADER_SOURCE_COOK   (SHADER_DIR "cook.vert");
  std::string const VERT_SHADER_SOURCE_COLOUR (SHADER_DIR "colour.vert");
  std::string const FRAG_SHADER_SOURCE_COLOUR (SHADER_DIR "colour.frag");

	int const SAMPLE_SIZE_WIDTH(256);
	int const SAMPLE_SIZE_HEIGHT(256);
	int const SAMPLE_MAJOR_VERSION(major);
	int const SAMPLE_MINOR_VERSION(minor);

	glf::window Window(glm::ivec2(SAMPLE_SIZE_WIDTH, SAMPLE_SIZE_HEIGHT));

	GLsizei const VertexCount(SAMPLE_SIZE_WIDTH * SAMPLE_SIZE_HEIGHT);
  GLuint iteratees = VertexCount;
  bool firstDisplay = true;
  int iterationcount = 0;
  flt cx = 0;
  flt cy = 0;
  flt r = 2;
  flt pixelspacing = flt(4) / SAMPLE_SIZE_WIDTH;

  GLuint IterateProgramName(0);
  GLuint IterateArrayBufferName(0);
  GLuint IterateVertexArrayName(0);

  GLuint EscapeProgramName(0);
  GLuint EscapeArrayBufferName(0);
  GLuint EscapeVertexArrayName(0);
  GLint  EscapeUniformEscape(0);

  GLuint CookProgramName(0);
  GLuint CookArrayBufferName(0);
  GLuint CookVertexArrayName(0);
  GLint  CookUniformIters(0);
  GLint  CookUniformScale(0);

  GLuint ColourProgramName(0);
  GLuint ColourArrayBufferName(0);
  GLuint ColourVertexArrayName(0);
  GLint  ColourUniformMVP(0);
  GLint  ColourUniformInterior(0);
  GLint  ColourUniformBorder(0);
  GLint  ColourUniformExterior(0);

  GLuint Query(0);

}//namespace

bool initProgram()
{
	bool Validated = true;
	
	// Create program 'iterate'
	//if(Validated)
	{
		GLuint VertexShaderName = glf::createShader(GL_VERTEX_SHADER, VERT_SHADER_SOURCE_ITERATE);

		IterateProgramName = glCreateProgram();
		glAttachShader(IterateProgramName, VertexShaderName);
		glDeleteShader(VertexShaderName);

		GLchar const * Strings[] = {"block.Position", "block.Color"}; 
		glTransformFeedbackVaryings(IterateProgramName, 2, Strings, GL_INTERLEAVED_ATTRIBS); 
		glLinkProgram(IterateProgramName);

		Validated = Validated && glf::checkProgram(IterateProgramName);

/*
		// BUG AMD 10.12
		char Name[64];
		memset(Name, 0, 64);
		GLsizei Length(0);
		GLsizei Size(0);
		GLenum Type(0);

		glGetTransformFeedbackVarying(
			IterateProgramName,
			0,
			64,
			&Length,
			&Size,
			&Type,
			Name);

		Validated = Validated && (Size == 1) && (Type == GL_flt_VEC4);
*/
	}


	// Create program 'escape'
	//if(Validated)
	{
		GLuint VertexShaderName = glf::createShader(GL_VERTEX_SHADER, VERT_SHADER_SOURCE_ESCAPE);
		GLuint GeometryShaderName = glf::createShader(GL_GEOMETRY_SHADER, GEOM_SHADER_SOURCE_ESCAPE);

		EscapeProgramName = glCreateProgram();
		glAttachShader(EscapeProgramName, VertexShaderName);
		glAttachShader(EscapeProgramName, GeometryShaderName);
		glDeleteShader(VertexShaderName);
		glDeleteShader(GeometryShaderName);

		GLchar const * Strings[] = {"block.Position", "block.Color"}; 
		glTransformFeedbackVaryings(EscapeProgramName, 2, Strings, GL_INTERLEAVED_ATTRIBS); 
		glLinkProgram(EscapeProgramName);

		Validated = Validated && glf::checkProgram(EscapeProgramName);
/*
		// BUG AMD 10.12
		char Name[64];
		memset(Name, 0, 64);
		GLsizei Length(0);
		GLsizei Size(0);
		GLenum Type(0);

		glGetTransformFeedbackVarying(
			EscapeProgramName,
			0,
			64,
			&Length,
			&Size,
			&Type,
			Name);

		Validated = Validated && (Size == 1) && (Type == GL_flt_VEC4);
*/
	}
	// Get variables locations
	//if(Validated)
	{
		EscapeUniformEscape = glGetUniformLocation(EscapeProgramName, "escape");
		Validated = Validated && (EscapeUniformEscape >= 0);
	}


	// Create program 'cook'
	//if(Validated)
	{
		GLuint VertexShaderName = glf::createShader(GL_VERTEX_SHADER, VERT_SHADER_SOURCE_COOK);

		CookProgramName = glCreateProgram();
		glAttachShader(CookProgramName, VertexShaderName);
		glDeleteShader(VertexShaderName);

		GLchar const * Strings[] = {"block.Position", "block.Color"}; 
		glTransformFeedbackVaryings(CookProgramName, 2, Strings, GL_INTERLEAVED_ATTRIBS); 
		glLinkProgram(CookProgramName);

		Validated = Validated && glf::checkProgram(CookProgramName);
/*
		// BUG AMD 10.12
		char Name[64];
		memset(Name, 0, 64);
		GLsizei Length(0);
		GLsizei Size(0);
		GLenum Type(0);

		glGetTransformFeedbackVarying(
			CookProgramName,
			0,
			64,
			&Length,
			&Size,
			&Type,
			Name);

		Validated = Validated && (Size == 1) && (Type == GL_flt_VEC4);
*/
	}
	// Get variables locations
	//if(Validated)
	{
		CookUniformIters = glGetUniformLocation(CookProgramName, "iters");
		CookUniformScale = glGetUniformLocation(CookProgramName, "scale");
		Validated = Validated && (CookUniformIters >= 0) && (CookUniformScale >= 0);
	}


  // Create program 'colour'
	//if(Validated)
	{
		GLuint VertexShaderName = glf::createShader(GL_VERTEX_SHADER, VERT_SHADER_SOURCE_COLOUR);
		GLuint FragmentShaderName = glf::createShader(GL_FRAGMENT_SHADER, FRAG_SHADER_SOURCE_COLOUR);

		ColourProgramName = glCreateProgram();
		glAttachShader(ColourProgramName, VertexShaderName);
		glAttachShader(ColourProgramName, FragmentShaderName);
		glDeleteShader(VertexShaderName);
		glDeleteShader(FragmentShaderName);

		glLinkProgram(ColourProgramName);
		Validated = Validated && glf::checkProgram(ColourProgramName);
	}
	// Get variables locations
	if(Validated)
	{
		ColourUniformMVP = glGetUniformLocation(ColourProgramName, "MVP");
		ColourUniformInterior = glGetUniformLocation(ColourProgramName, "interior");
		ColourUniformBorder = glGetUniformLocation(ColourProgramName, "border");
		ColourUniformExterior = glGetUniformLocation(ColourProgramName, "exterior");

		Validated = Validated && (ColourUniformMVP >= 0) && (ColourUniformInterior >= 0) && (ColourUniformBorder >= 0) && (ColourUniformExterior >= 0);
	}

	return Validated && glf::checkError("initProgram");
}

void initVertexArray1(GLuint *name, GLuint arr, const char *err) {
glf::checkError("iva 1");
	glGenVertexArrays(1, name);
glf::checkError("iva 2");
    glBindVertexArray(*name);
glf::checkError("iva 3");
		glBindBuffer(GL_ARRAY_BUFFER, arr);
glf::checkError("iva 4");
		glVertexAttribPointer_(glf::semantic::attr::POSITION, 4, GL_FALSE, sizeof(point), 0);
glf::checkError("iva 5");
		glBindBuffer(GL_ARRAY_BUFFER, arr);
glf::checkError("iva 6");
		glVertexAttribPointer_(glf::semantic::attr::COLOR, 4, GL_FALSE, sizeof(point), GLF_BUFFER_OFFSET(sizeof(vec4)));
glf::checkError("iva 6");
		glBindBuffer(GL_ARRAY_BUFFER, 0);
glf::checkError("iva 8");
		glEnableVertexAttribArray(glf::semantic::attr::POSITION);
glf::checkError("iva 9");
		glEnableVertexAttribArray(glf::semantic::attr::COLOR);
glf::checkError("iva A");
	glBindVertexArray(0);
glf::checkError("iva B");
	glf::checkError(err);
}

bool initVertexArray() {
  glf::checkError("initVertexArray 0");
  initVertexArray1(&IterateVertexArrayName, IterateArrayBufferName, "iva iterate");
  initVertexArray1(&EscapeVertexArrayName , EscapeArrayBufferName , "iva escape");
  initVertexArray1(&CookVertexArrayName   , CookArrayBufferName   , "iva cook");
  initVertexArray1(&ColourVertexArrayName , ColourArrayBufferName , "iva colour");
  return glf::checkError("initVertexArray");
}

void initArrayBuffer1(GLuint *name, const char *err, void *data) {
	glGenBuffers(1, name);
  glBindBuffer(GL_ARRAY_BUFFER, *name);
    glBufferData(GL_ARRAY_BUFFER, sizeof(point) * VertexCount, data, GL_DYNAMIC_COPY);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
  glf::checkError(err);
}  

void resetView() {
  point *positions = (point *) calloc(1, sizeof(point) * VertexCount);
  point *p = positions;
  for (int i = 0; i < SAMPLE_SIZE_WIDTH; ++i) {
    flt x = cx + (i - SAMPLE_SIZE_WIDTH/2) * r / (SAMPLE_SIZE_WIDTH/2);
    for (int j = 0; j < SAMPLE_SIZE_HEIGHT; ++j) {
      flt y = cy + (j - SAMPLE_SIZE_HEIGHT/2) * r / (SAMPLE_SIZE_HEIGHT/2);
      *p[0] = vec4(x, y, flt(0), flt(1));
      p++;
    }
  }
glf::checkError("view 1");
  glBindBuffer(GL_ARRAY_BUFFER, IterateArrayBufferName);
glf::checkError("view 2");
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(point) * VertexCount, positions);
glf::checkError("view 3");
  glBindBuffer(GL_ARRAY_BUFFER, 0);
glf::checkError("view 4");
  free(positions);
  iterationcount = 0;
  iteratees = VertexCount;
}

bool initArrayBuffer()
{
  glf::checkError("initArrayBuffer 0");
  void *zero = calloc(1, sizeof(point) * VertexCount);
	initArrayBuffer1(&IterateArrayBufferName, "iab iterate", zero);
  free(zero);
	initArrayBuffer1(&EscapeArrayBufferName, "iab escape", NULL);
	initArrayBuffer1(&CookArrayBufferName, "iab cook", NULL);
	initArrayBuffer1(&ColourArrayBufferName, "iab colour", NULL);
	return glf::checkError("initArrayBuffer");
}

void click(int button, int state, int x, int y) {
  if (!state) {
    cx += (x - SAMPLE_SIZE_WIDTH /2) * r / (SAMPLE_SIZE_WIDTH /2);
    cy += (SAMPLE_SIZE_HEIGHT/2 - y) * r / (SAMPLE_SIZE_HEIGHT/2);
    if (!button) {
      r  /= 2;
      pixelspacing /= 2;
    } else {
      r  *= 2;
      pixelspacing *= 2;
    }
    resetView();
  } 
}

bool begin()
{
	bool Validated = true;
	Validated = Validated && glf::checkGLVersion(SAMPLE_MAJOR_VERSION, SAMPLE_MINOR_VERSION);
	Validated = Validated && glf::checkExtension("GL_ARB_viewport_array");
	Validated = Validated && glf::checkExtension("GL_ARB_separate_shader_objects");

/*
  int sep = 0;
  int ilv = 0;
  glGetIntegerv(GL_MAX_TRANSFORM_FEEDBACK_SEPARATE_COMPONENTS, &sep);
  glGetIntegerv(GL_MAX_TRANSFORM_FEEDBACK_INTERLEAVED_COMPONENTS, &ilv);
  printf("%d %d\n", sep, ilv);
*/

  glClampColor(GL_CLAMP_VERTEX_COLOR, GL_FALSE);
  glClampColor(GL_CLAMP_READ_COLOR, GL_FALSE);
  glClampColor(GL_CLAMP_FRAGMENT_COLOR, GL_FALSE);

glf::checkError("begin 1");
	glGenQueries(1, &Query);
glf::checkError("begin 2");
	if(Validated)
		Validated = initProgram();
glf::checkError("begin 3");
	if(Validated)
		Validated = initArrayBuffer();
glf::checkError("begin 4");
	if(Validated)
		Validated = initVertexArray();
glf::checkError("begin 5");

  resetView();
  
	return Validated && glf::checkError("begin");
}

bool end()
{
/* // FIXME
	glDeleteVertexArrays(1, &TransformVertexArrayName);
	glDeleteBuffers(1, &TransformArrayBufferName);
	glDeleteProgram(TransformProgramName);
	
	glDeleteVertexArrays(1, &FeedbackVertexArrayName);
	glDeleteBuffers(1, &FeedbackArrayBufferPositionName);
	glDeleteBuffers(1, &FeedbackArrayBufferColorName);
	glDeleteProgram(FeedbackProgramName);
*/
	glDeleteQueries(1, &Query);

	return glf::checkError("end");
}

void timer1(int v) {
  glutPostRedisplay();
}

void display()
{
for (int repeats = 0; repeats < 10; ++repeats) {

	// Compute the MVP (Model View Projection matrix)
	mat4 Projection = glm::ortho(cx - r, cx + r, cy - r, cy + r);
	mat4 View = IDENTITY;
	mat4 Model = IDENTITY;
	mat4 MVP = Projection * View * Model;

	// Set the display viewport
	glViewport(0, 0, Window.Size.x, Window.Size.y);

  if (iterationcount == 0) {
	  // Clear color buffer with red
  	glClearColor(1.0f, 0.0f, 0.0f, 1.0f);
  	glClear(GL_COLOR_BUFFER_BIT);
  	glf::checkError("clear");
  }

  // Disable rasterisation, vertices processing only!
	glEnable(GL_RASTERIZER_DISCARD);

	// iterate
	{
    glUseProgram(IterateProgramName);
    // dst
/*
void glBindBufferRange(	GLenum 	target, 
 	GLuint 	index, 
 	GLuint 	buffer, 
 	GLintptr 	offset, 
 	GLsizeiptr 	size);
*/
		glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, EscapeArrayBufferName);
//		glBindBufferRange(GL_TRANSFORM_FEEDBACK_BUFFER, 1, EscapeArrayBufferName, sizeof(vec4), sizeof(point) * VertexCount);
//		glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 1, EscapeArrayBufferColorName);
    // src
		glBindVertexArray(IterateVertexArrayName);
    // do it
		glBeginTransformFeedback(GL_POINTS);
		glDrawArrays(GL_POINTS, 0, iteratees);
		glEndTransformFeedback();
		glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, 0);
//		glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 1, 0);
		glBindVertexArray(0);
  	glf::checkError("iterate");
  }
  // keep unescaped
  GLuint unescaped = 0;
	{
    glUseProgram(EscapeProgramName);
  	glf::checkError("unescaped 1");
    glUniform1i(EscapeUniformEscape, 0);
  	glf::checkError("unescaped 2");
    // dst
		glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, IterateArrayBufferName);
  	glf::checkError("unescaped 3");
//		glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 1, IterateArrayBufferColorName);
  	glf::checkError("unescaped 4");
    // src
		glBindVertexArray(EscapeVertexArrayName);
  	glf::checkError("unescaped 5");
    // do it
		glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, Query); 
  	glf::checkError("unescaped 6");
		glBeginTransformFeedback(GL_POINTS);
  	glf::checkError("unescaped 7");
			glDrawArrays(GL_POINTS, 0, iteratees);
  	glf::checkError("unescaped 8");
		glEndTransformFeedback();
  	glf::checkError("unescaped 9");
		glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN); 
  	glf::checkError("unescaped A");
		glGetQueryObjectuiv(Query, GL_QUERY_RESULT, &unescaped);
  	glf::checkError("unescaped");
  }
  // pass through escaped
  GLuint escaped = 0;
	{
    glUseProgram(EscapeProgramName);
    glUniform1i(EscapeUniformEscape, 1);
    // dst
		glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, CookArrayBufferName);
//		glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 1, CookArrayBufferColorName);
    // src
		glBindVertexArray(EscapeVertexArrayName);
    // do it
		glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, Query); 
		glBeginTransformFeedback(GL_POINTS);
			glDrawArrays(GL_POINTS, 0, iteratees);
		glEndTransformFeedback();
		glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN); 
		glGetQueryObjectuiv(Query, GL_QUERY_RESULT, &escaped);
  	glf::checkError("escaped");
  }
  // cook the escapees
	{
    glUseProgram(CookProgramName);
    glUniform1f_(CookUniformIters, ++iterationcount);
    glUniform1f_(CookUniformScale, pixelspacing);
    // dst
		glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, ColourArrayBufferName);
//		glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 1, ColourArrayBufferColorName);
    // src
		glBindVertexArray(CookVertexArrayName);
    // do it
		glBeginTransformFeedback(GL_POINTS);
			glDrawArrays(GL_POINTS, 0, escaped);
		glEndTransformFeedback();
  	glf::checkError("cook");
  }

  // re-enable drawing
  glDisable(GL_RASTERIZER_DISCARD);

  // colourize the cookeds
	{
		glUseProgram(ColourProgramName);
		glUniformMatrix4fv_(ColourUniformMVP, 1, GL_FALSE, &MVP[0][0]);
		glUniform3f(ColourUniformInterior, 1.0, 0.0, 0.0);
		glUniform3f(ColourUniformBorder  , 0.0, 0.0, 0.0);
		glUniform3f(ColourUniformExterior, 1.0, 1.0, 1.0);
		glBindVertexArray(ColourVertexArrayName);
		glDrawArrays(GL_POINTS, 0, escaped);
  	glf::checkError("colour");
	}
  if ((iterationcount % 1000) == 0) {
    printf("%d\t%d\t%d\t%d\n", iterationcount, iteratees, escaped, unescaped);
  }
  iteratees = unescaped;

	glf::checkError("display");
  if ((iterationcount % 100) == 0) {
  	glf::swapBuffers();
  }
}
  glutTimerFunc(1, timer1, 1);
  glutIdleFunc(0);
  glutMouseFunc(click);
}

int main(int argc, char* argv[])
{
	return glf::run(
		argc, argv,
		glm::ivec2(::SAMPLE_SIZE_WIDTH, ::SAMPLE_SIZE_HEIGHT), 
		::SAMPLE_MAJOR_VERSION, 
		::SAMPLE_MINOR_VERSION);
}
