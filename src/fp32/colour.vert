#version 330 core
precision highp float;

#define POSITION	0
#define COLOR		3
#define FRAG_COLOR	0

uniform mat4 MVP;

uniform vec3 interior;
uniform vec3 border;
uniform vec3 exterior;

layout(location = POSITION) in vec4 Position;
layout(location = COLOR) in vec4 Color;

out block {
  vec4 Color;
} Out;

void main() {
  float n = Color.x;
  float d = Color.y;
//float a = Color.z;
  vec4 c = vec4(interior, 1.0);
  if (n > 0.0) {
    float k = clamp(0.5 + 0.5 * log2(0.5 * d), 0.0, 1.0);
    c.rgb = mix(border, exterior, k);
  }
  gl_Position = MVP * Position;
  Out.Color = c;
}
