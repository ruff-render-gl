#version 330 core
precision highp float;

#define POSITION	0
#define COLOR		3
#define FRAG_COLOR	0

layout(location = POSITION) in vec4 Position;
layout(location = COLOR) in vec4 Color;

out vec4 OutPosition;
out vec4 OutColor;

void main() {
  OutPosition = Position;
  OutColor = Color;
}
