#version 330 core
#extension GL_ARB_separate_shader_objects : enable
precision highp float;

#define POSITION	0
#define COLOR		3
#define FRAG_COLOR	0

layout(points) in;
layout(points, max_vertices = 1) out;

layout(location = POSITION) in vec4 OutPosition[1];
layout(location = COLOR) in vec4 OutColor[1];

uniform bool escape;

out block {
	vec4 Position;
	vec4 Color;
} Out;

const float er2 = 4.0;

void main() {
	vec2 z = OutColor[0].xy;
	float zx2 = z.x * z.x;
	float zy2 = z.y * z.y;
	float z2 = zx2 + zy2;
  if (escape == !(z2 < er2)) {
		Out.Position = OutPosition[0];
		Out.Color = OutColor[0];
		EmitVertex();
		EndPrimitive();
	}
}
