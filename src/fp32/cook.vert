#version 330 core
precision highp float;

#define POSITION	0
#define COLOR		3
#define FRAG_COLOR	0

layout(location = POSITION) in vec4 Position;
layout(location = COLOR) in vec4 Color;

out block {
	vec4 Color;
	vec4 Position;
} Out;

const float er2 = 65536.0;

uniform float iters;
uniform float scale;

void main() {
	vec2 c = Position.xy;
	vec2 z = Color.xy;
	vec2 dz = Color.zw;
	float zx2 = z.x * z.x;
	float zy2 = z.y * z.y;
	float z2 = zx2 + zy2;
	float z2xy = 2.0 * z.x * z.y;
	float dz2 = dz.x * dz.x + dz.y * dz.y;
  float n = 1.0 + iters - log2(log(z2) / log(er2));
  float d = log(z2) * sqrt(z2 / dz2) / scale;
  float a = atan(z.y, z.x);
	Out.Position = Position;
	Out.Color = vec4(n, d, a, 1.0);
}
