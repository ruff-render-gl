#version 330 core

#define POSITION	0
#define COLOR		3
#define FRAG_COLOR	0

layout(location = POSITION) in vec4 Position;
layout(location = COLOR) in vec4 Color;

out block {
	vec4 Color;
	vec4 Position;
} Out;

const float er2 = 65536.0;

void main() {
	vec2 c = Position.xy;
	vec2 z = Color.xy;
	vec2 d = Color.zw;
	float zx2 = z.x * z.x;
	float zy2 = z.y * z.y;
	float z2 = zx2 + zy2;
	float z2xy = 2.0 * z.x * z.y;
//	if (z2 < er2) {
	float zdzx = z.x * d.x - z.y * d.y;
	float zdzy = z.x * d.y + z.y * d.x;
	float dx = 2.0 * zdzx + 1.0;
	float dy = 2.0 * zdzy;
  float zx = zx2 - zy2 + c.x;
  float zy = z2xy + c.y;
	Out.Position = Position;
	Out.Color = vec4(zx, zy, dx, dy);
}
